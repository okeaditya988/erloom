<?php

namespace App\Http\Controllers;

use App\UserTest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class TestController extends Controller
{
    public function index()
    {
        $data = DB::table('user_tests')->get();
        return view('index',compact('data'));
    }

    public function store(Request $request)
    {
        $data = DB::table('user_tests')->latest()->first();
        if(!$data){
            $id = 0;
            $parity = 'Even';
        }else{
            if($data->id%2==0){
                $parity = 'Odd';
            }else{
                $parity = 'Even';
            }
            $id = $data->id+1;
        }


        try {
            $store = new UserTest;
            $store->id = $id;
            $store->name = $request->name;
            $store->parity = $parity;
            $store->save();
         } catch (\Exception $e) {

         }
        return redirect()->route('index');
    }

}
